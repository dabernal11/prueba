package model.data_structures;

/**
 * 2019-01-23
 * Estructura de Datos Arreglo Dinamico de Strings.
 * El arreglo al llenarse (llegar a su maxima capacidad) debe aumentar su capacidad.
 * @author Fernando De la Rosa
 *
 */
public class ArregloDinamico implements IArregloDinamico {
		/**
		 * Capacidad maxima del arreglo
		 */
        private int tamanoMax;
		/**
		 * Numero de elementos en el arreglo (de forma compacta desde la posicion 0)
		 */
        private int tamanoAct;
        /**
         * Arreglo de elementos de tamaNo maximo
         */
        private String elementos[ ];

        /**
         * Construir un arreglo con la capacidad maxima inicial.
         * @param max Capacidad maxima inicial
         */
		public ArregloDinamico( int max )
        {
               elementos = new String[max];
               tamanoMax = max;
               tamanoAct = 0;
        }
        
		public void agregar( String dato )
        {
               if ( tamanoAct == tamanoMax )
               {  // caso de arreglo lleno (aumentar tamaNo)
            	   
                    tamanoMax = 2 * tamanoMax;
                    String [ ] copia = elementos;
                    elementos = new String[tamanoMax];
                    for ( int i = 0; i < tamanoAct; i++)
                    {
                     	 elementos[i] = copia[i];
                    } 
            	    System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
               }	
               elementos[tamanoAct] = dato;
               tamanoAct++;
       }

		public int darTamano() {
			
			return tamanoAct;
		}

		public String darElemento(int i) {
			String respuesta = "";
			
			if(i >= tamanoAct)
			{
				respuesta = elementos[i];
			}
			return respuesta;
		}

		public String buscar(String dato) {
			String respuesta = "";
			
			for (int i = 0; i < tamanoAct; i++) 
			{
				if(dato.compareTo(elementos[i]) == 0)
				{
					respuesta = elementos[i];
				}
			}
			return respuesta;
		}

		public String eliminar(String dato) {
			
			String respuesta = "";
			boolean eliminado = false;
			
			for(int  i=0; i < elementos.length && eliminado != true; i++) 
			{
				if(elementos[i]!= null && elementos[i].equals(dato)) 
				{
					respuesta = elementos [i];
					elementos[i] = null;
					eliminado = true;
				}	
			}
			return respuesta;
		}
}
